﻿
USE master
CREATE DATABASE [TestSqlDep]
ALTER DATABASE [TestSqlDep] SET ENABLE_BROKER
CREATE TABLE [TestSqlDep].[dbo].[TestTable] (
	ID INT PRIMARY KEY IDENTITY,
	Col01 VARCHAR(50) NULL
)


/*
INSERT INTO TestSqlDep.dbo.TestTable VALUES(GETDATE())
*/