﻿using ServiceStack.Data;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace SqlDependencyTest
{
    public class CachedData<T>
    {
        private IDbConnectionFactory _connFactory;
        private SqlDependency _sqlDependency;
        public List<T> Data { get; private set; }

        public CachedData(IDbConnectionFactory connFactory)
        {
            _connFactory = connFactory;
            RehydrateData();
        }

        private void RehydrateData()
        {
            using (var conn = _connFactory.OpenDbConnection())
            {
                var query = conn.From<T>();
                var cmd = conn.CreateCommand();

                cmd.CommandText = query.ToSelectStatement();
                cmd.CommandType = CommandType.Text;

                _sqlDependency = new SqlDependency((SqlCommand)cmd);
                _sqlDependency.OnChange += Dep_OnChange;

                Data = cmd.ExecuteReader().ConvertToList<T>(conn.GetDialectProvider());
            }
        }

        private void Dep_OnChange(object sender, SqlNotificationEventArgs e)
        {
            var dep = (SqlDependency)sender;
            dep.OnChange -= Dep_OnChange;

            Console.WriteLine();
            if (e.Info == SqlNotificationInfo.Invalid)
            {
                Console.WriteLine(">>The above notification query is not valid.");
            }
            else
            {
                Console.WriteLine(">>Notification Info: " + e.Info);
                Console.WriteLine(">>Notification source: " + e.Source);
                Console.WriteLine(">>Notification type: " + e.Type);
            }

            RehydrateData();
        }
    }
}
