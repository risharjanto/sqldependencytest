﻿using ServiceStack.Data;
using ServiceStack.DataAnnotations;
using ServiceStack.OrmLite;
using ServiceStack.Messaging;
using ServiceStack;
using System;
using System.Data;
using System.Data.SqlClient;
using ServiceStack.OrmLite.SqlServer;
using System.Threading.Tasks;

namespace SqlDependencyTest
{
    public class Program
    {
        [Schema("dbo")]
        [Alias("TestTable")]
        private class TestTable
        {
            [AutoIncrement]
            [Required]
            public int ID { get; set; }
            public string Col01 { get; set; }
        }

        private const string CONNECTION_STRING = "Data Source=localhost;Initial Catalog=TestSqlDep;Integrated Security=True;Persist Security Info=False;Pooling=False;MultipleActiveResultSets=False;Connect Timeout=60;Encrypt=False;TrustServerCertificate=True";

        private CachedData<TestTable> _cachedTestTable;

        public async Task Run()
        {
            SqlDependency.Start(CONNECTION_STRING);
            try
            {
                var converter = new SqlServer2014OrmLiteDialectProvider();
                converter.GetStringConverter().UseUnicode = true;
                converter.RegisterConverter<TimeSpan>(new ServiceStack.OrmLite.SqlServer.Converters.SqlServerTimeConverter
                {
                    Precision = 7
                });
                var connFact = new OrmLiteConnectionFactory(CONNECTION_STRING, converter);

                _cachedTestTable = new CachedData<TestTable>(connFact);

                UserInput(); //it's going to keep waiting for user input here. ( until (Q)uit )
            }
            finally
            {
                SqlDependency.Stop(CONNECTION_STRING);
            }
        }

        private void UserInput()
        {
            while (true) {
                Console.WriteLine("********************************");
                Console.Write(@"Press: 
    C to create/insert(db), 
    R to read(cache), 
    U to update(db),
    D to delete(db), 
    T to truncate(db), 
    Q to quit
>> ");

                var readKey = Console.ReadKey();
                Console.WriteLine();
                Console.WriteLine("********************************");

                switch (readKey.KeyChar)
                {
                    case 'I':
                    case 'i':
                    case 'C':
                    case 'c':
                        using (var conn = new SqlConnection(CONNECTION_STRING))
                        {
                            conn.Open();

                            var statement = "INSERT INTO TestSqlDep.dbo.TestTable VALUES(NEWID())";
                            var cmd = new SqlCommand(statement, conn);
                            cmd.ExecuteNonQuery();

                        }
                        break;
                    case 'R':
                    case 'r':
                        var data = _cachedTestTable.Data;
                        Console.WriteLine("Cache Results:");
                        foreach (var row in data)
                        {
                            Console.WriteLine($"    {row.ID},   {row.Col01}");
                        }
                        Console.WriteLine();
                        break;
                    case 'D':
                    case 'd':
                        using (var conn = new SqlConnection(CONNECTION_STRING))
                        {
                            conn.Open();

                            var statement = "DELETE TOP (1) FROM TestSqlDep.dbo.TestTable";
                            var cmd = new SqlCommand(statement, conn);
                            cmd.ExecuteNonQuery();

                        }
                        break;
                    case 'U':
                    case 'u':
                        using (var conn = new SqlConnection(CONNECTION_STRING))
                        {
                            conn.Open();

                            var statement = "UPDATE TestSqlDep.dbo.TestTable SET Col01 = NEWID()";
                            var cmd = new SqlCommand(statement, conn);
                            cmd.ExecuteNonQuery();

                        }
                        break;
                    case 'T':
                    case 't':
                        using (var conn = new SqlConnection(CONNECTION_STRING))
                        {
                            conn.Open();

                            var statement = "TRUNCATE TABLE TestSqlDep.dbo.TestTable";
                            var cmd = new SqlCommand(statement, conn);
                            cmd.ExecuteNonQuery();

                        }
                        break;
                    case 'Q':
                    case 'q':
                        return;
                    default:
                        Console.WriteLine("Unknown key");
                        break;
                }
                
                
            }

        }

        static void Main(string[] args)
        {
            new Program().Run();

        }
    }
}
